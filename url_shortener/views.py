# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
import random, string, json
import logging
import urlparse
import requests
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from django.shortcuts import render, get_object_or_404
from url_shortener.models import URLs
from django.http import HttpResponseRedirect, HttpResponse
from django.conf import settings
from django.views.decorators.csrf import csrf_protect
from ratelimit.decorators import ratelimit

logger = logging.getLogger('django')

@csrf_protect
def index(request):
    """Index page"""
    context = {}
    return render(request, 'url_shortener/index.html', context)

def phish_error(request):
    """Phish error page"""
    context = {}
    return render(request, 'url_shortener/phish-error.html', context)

def redirect_original(request, short_id):
    """Gets object or returns 404 not found error"""
    url = get_object_or_404(URLs, pk=short_id)
    url.count += 1
    url.save()
    # Redirects to phish error page if a Phish URL is detected
    if safeguard_url(url.http_url):
        return HttpResponseRedirect('/phish-error/')
    else:
        return HttpResponseRedirect(url.http_url)

def safeguard_url(url):
    """Safeguards the url using resources from https://www.phishtank.com."""
    data = {'url': url,
            'format': 'json',
            'app_key': settings.APP_KEY}
    try:
        r = requests.post(url=settings.APP_URL, data=data)
        results = json.loads(r.text).get('results')
        if results and results.get('in_database', False) and results.get('phish_id', None) and results.get('valid', False):
            return True
        else:
            return False
    except Exception as e:
        logger.error("Error when making the request to safeguard the url: {}".format(e.message))
        return True

@ratelimit(key='user_or_ip', rate='50/s', block=True, method='POST')
def shorten_url(request):
    """Shortens the provided URL."""
    try:
        url = request.POST.get('url', '')
        short_id = None
        if url:
            response_data = {}
            https = 'http' if 'http:' in url else 'https'
            p = urlparse.urlparse(url, https)
            netloc = p.netloc or p.path
            path = p.path if p.netloc else ''
            p = urlparse.ParseResult(https, netloc, path, *p[3:])
            url = p.geturl()
            try:
                url_obj = URLs.objects.get(http_url=url)
                short_id = url_obj.short_id
            except ObjectDoesNotExist as e:
                logger.error(e.message)
            except MultipleObjectsReturned as e:
                logger.error(e.message)
                URLs.objects.filter(http_url=url).delete()
            if not short_id:
                short_id = get_short_id()
                URLs.objects.create(http_url=url, short_id=short_id)
            if safeguard_url(url):
                response_data['error'] = 'phishing'
            response_data['url'] = settings.SITE_URL + '/' + short_id
            return HttpResponse(json.dumps(response_data),  content_type='application/json')
        else:
            return HttpResponse(json.dumps({'error': 'The given URL is empty.'}), content_type='application/json')
    except Exception as e:
        logger.error(e.message)
        return HttpResponse(json.dumps({'error': e.message}), content_type='application/json')
 
def get_short_id():
    """Creates a new short_id for a URL."""
    length = 6
    char = string.ascii_uppercase + string.digits + string.ascii_lowercase
    # If the randomly generated short_id is used, then returns it
    while True:
        short_id = ''.join(random.choice(char) for x in range(length))
        try:
            temp = URLs.objects.get(pk=short_id)
        except:
            return short_id
