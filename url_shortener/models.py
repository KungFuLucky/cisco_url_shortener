# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class URLs(models.Model):
    short_id = models.SlugField(max_length=6, primary_key=True)
    http_url = models.URLField(max_length=1024)
    pub_date = models.DateTimeField(auto_now=True)
    count = models.IntegerField(default=0)
    
    class Meta:
        ordering = ['pub_date']
        verbose_name = 'URL'
        verbose_name_plural = 'URLs'

def __str__(self):
    return self.http_url
