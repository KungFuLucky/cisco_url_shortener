# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from url_shortener.models import URLs
# Register your models here.
 
class URLsAdmin(admin.ModelAdmin):
    list_display = ('short_id','http_url','pub_date', 'count')
    ordering = ('-pub_date',)
 
admin.site.register(URLs, URLsAdmin)
