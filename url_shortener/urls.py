"""cisco_url_shortener URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views
 
urlpatterns = [
    url(r'^$', views.index, name='home'),
    url(r'^phish-error/?$', views.phish_error, name='phish_error'),
    # Redirects the short URL to the original URL
    url(r'^(?P<short_id>\w{6})$', views.redirect_original, name='redirect_original'),
    # Creates a URL's short id and return the short URL
    url(r'^shorten/?$', views.shorten_url, name='shorten_url'),
]
